# Example Minio Deployment in Spin 

## Rancher Steps

1. Create namespace in rancher: `minio`

2. Setup data directory

```
mkdir /global/cfs/cdirs/das/minio-test/data
chgrp das /global/cfs/cdirs/das/minio-test/data
chmod -R 2775 /global/cfs/cdirs/das/minio-test/data
```

3. Deploy workload


UID is your your UID
GID is group GID or your GID

Example config
```
name: minio-test
image: minio/minio
namespace: minio
env:
  MINIO_ACCESS_KEY: <admin_username> # use minio_admin
  MINIO_SECRET_KEY: <password>
volumes:
  name: minio-data
  type: bind-mount
  path: /global/cfs/cdirs/das/minio-test/data
  validation: An Existing Directory
  mount-point: /data
command
  command: server data --console-address ":9001"
  uid: 16489 # use project account?
  gid: 65982 # das
run-as: non-root
caps:
  add: 
    - NET_BIND_SERVICE
  drop: 
    - all
```


4. Ingress
```
name: app
namespace: minio
hostname: app.minio.development.svc.spin.nersc.org # <name>.<namespace>.<env>.svc.spin.nersc.org
backend:
  target: minio-test # workload name
  port: 9000 # minio default
hostname: console.minio.development.svc.spin.nersc.org # <name>.<namespace>.<env>.svc.spin.nersc.org
backend:
  target: minio-test # workload name
  port: 9001 # minio default
certificates
  default ingress
  host: app.minio.development.svc.spin.nersc.org
certificates
  default ingress
  host: console.minio.development.svc.spin.nersc.org
annotations
  nginx.ingress.kubernetes.io/proxy-body-size: 2000m
  nginx.ingress.kubernetes.io/server-snippet: | 
    ignore_invalid_headers off; # this looks different in yaml - can drop the | in rancher UI
```
  
Note that you need to set up a separate hostname in the ingress for the admin web console since it runs on a different port (9001 above) with newer versions of minio

## MinIO Setup


### Install minio `mc` client
```
# MacOS
brew install minio/stable/mc
```
or go here: https://min.io/download


### Configure minio

```
minio_url="https://app.minio.development.svc.spin.nersc.org/"
minio_admin_user=minio_admin 
minio_admin_pass="password"
```

```
mc alias set spin-minio $minio_url $minio_admin_user $minio_admin_pass --api s3v4
```


### Add policies

Create a bucket with a custom name (eg. `mybucket`) and edit policy_bucket_*.json with mybucket 

We basically create two styles of policies
  - Policies for buckets named for the user (ro/rw)
  - Policies for arbitrary buckets created (ro/rw)

```
# Add --insecure if certs are not signed
mc admin policy add spin-minio userbucket_ro policy_userbucket_ro.json
mc admin policy add spin-minio userbucket_rw policy_userbucket_rw.json
mc admin policy add spin-minio bucket_ro policy_bucket_ro.json
mc admin policy add spin-minio bucket_rw policy_bucket_rw.json
```


# Create user and assign policy

```
$username=foo
$password=bar
```

Create User
```
# Add --insecure if certs are not signed
mc admin user add spin-minio $username $password 
```

Example Policy to assign. 
eg. rw on user bucket 
```
# Add --insecure if certs are not signed
mc admin policy set spin-minio userbucket_rw user=$username
```

Make a bucket available for download
```
# Add --insecure if certs are not signed
mc policy set download spin-minio/foo
```
Note: Do not set to `upload` or `public` since that allows public upload. 

you can only do one policy per user - so you'll need to tweak the policy to meet your needs.




